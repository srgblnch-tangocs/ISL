//*******************************************************************************
//* Copyright (c) 2004-2014 Synchrotron SOLEIL
//* All rights reserved. This program and the accompanying materials
//* are made available under the terms of the GNU Lesser Public License v3
//* which accompanies this distribution, and is available at
//* http://www.gnu.org/licenses/lgpl.html
//******************************************************************************
#include <isl/blob/BlobComparator.h>

#if !defined (__ISL_INLINE__)
# include <isl/blob/BlobComparator.i>
#endif

namespace isl {




}
